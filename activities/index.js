// Test Output
console.log('Hello World!');

// Instruction #3:
let numOne = 7;
let numTwo = 23;

// SUM
let sum = numOne + numTwo;
console.log("The sum of the two numbers is: " + sum);

// DIFFERENCE
let difference = numOne - numTwo;
console.log("The difference of the two numbers is: " + difference);

// PRODUCT
let product = numOne * numTwo;
console.log("The product of the two numbers is: " + product);

// QUOTIENT
let quotient = numOne / numTwo;
console.log("The quotient of the two numbers is: " + quotient);

// Instruction #4 (Comparison Operators)
// sum GREATER THAN difference
let firstComparison = sum > difference;
console.log("The sum is greater than the difference: " + firstComparison);

// product && quotient are BOTH POSITIVE NUMBERS
let arePositiveNumbers = (product > 0) && (quotient > 0);
console.log("The product and quotient are positive numbers: " + arePositiveNumbers);

// one of the results is a negative number
let areResultsNegative = (sum < 0 || difference < 0 || product < 0 || quotient < 0) ;
console.log("One of the results is negative: " + areResultsNegative);

// one of the results is ZERO
let areResultsZero = (sum !== 0 || difference !== 0 || product !==0 || quotient !== 0);
console.log("All the results are not equal to zero: " +  areResultsZero);