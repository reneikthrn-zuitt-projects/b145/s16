console.log('Hello from JS');

/* 
	[Section 1] 
	Assignment Operators
	1.) Basic Assignment Operator (=)
		> This allows us to add the value or the right operand to a variable and assigns the result to the variable.

	2.) Addition Assignment Operator (+=)
		> The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
*/

// =
let assignmentNumber = 5;
let message = 'This is the message';

// +=
assignmentNumber += 2; // assignmentNumber = assignmentNumber + 2;
console.log("Result of the operation: " + assignmentNumber);


/*
	[Section 2]
	Arithmetic Operators
	3.) Subtraction/Multiplication/Division Assignment Operator (-=), (*=), (/=)
*/
assignmentNumber -= 3;
console.log("Result of the operation: " + assignmentNumber);


// ----- [RECAP] Assignment Operators -----
let value = 8;

// Addition Assignment (+=)
// value += 15;

// Subtraction Assignment (-=)
// value -=5 ;

// Multiplication Assignment (*=)
// value *= 2;

// Division Assignment (/=)
// value /= 2;

// console.log(value);


// [Section 2] Arithmetic Operators
let x = 15;
let y = 10;

// addition (+)
let sum = x + y;
console.log(sum);

// subtraction (-)
let difference = x - y;
console.log(difference);

// multiplication (*)
let product = x * y;
console.log(product);

// division (/)
let quotient = x / y;
console.log(quotient);

// Remainder between the 2 values = Modulus (%)
let remainder = x % y;
console.log(remainder);


/* 
	[Subsection 2.1] Multiple Operators and Parenthesis
	=> When multiple operators are applied in a single statement, it follows the PEMDAS rule. (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*
	The operation were done in the following order to get to the final answer
		1.) 3 * 4 = 12		(Multiplication)
		2.) 12 / 5 = 2.4	(Division)
		3.) 1 + 2 = 3		(Addition)
		4.) 3 - 2.4 = 0.6	(Subtraction)
*/

// !NOTE: The order of opertions can be changed by adding parenthesis to the logic

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

/*
	By adding parenthesis (), the order of the operations are changed prioritizing the operations enclosed within parenthesis.
	This operation was done with the following order:
		1.) 4 / 5 = 0.8 and
			2 - 3 = -1
		2.) -1 * 0.8 = -0.8
		3.) 1 + -.08 = .2
*/


/* 
	[Section 3]
	Incement (++) and Decrement (--)
	2 TYPES:
		1.) PRE
		2.) POST
*/

// The value of "z" is added by a value of 1 before returning the value and storing it inside a new variable: "preIncrement".
let z = 1;

// Pre-Increment (syntax: ++var)
let preIncrement = ++z;

console.log(preIncrement);
// we can see here that the value of z was also increased even though we did not implicitly specified any variable reassignment.
console.log(z);


// Post-Increment (syntax: var++)
let postIncrement = z++;

// The value of z is returned and stored inside the variable called "postIncrement". The value of z is at 2 before it was incremented.
console.log(postIncrement);

console.log(z);


// Pre-Decrement (syntax: --var)
// The value of Z starts with 3 before it was decremented
let preDecrement = --z;
console.log(preDecrement);


// Post-Decrement (syntax: var--)
let postDecrement = z--;
// The value of 'z' is returned and stored inside a variable before it will be decremented
console.log(postDecrement);
console.log(z);


/*
	[Section 4]
	Type Coercion
*/
let numberA = 6;
let numberB = '6';

// let's check the data types of the values above
// typof expression => will allow us to identify the data type of a certain value/component
console.log(typeof numberA); // number
console.log(typeof numberB); // string

// number + string => number data type was converted into a string to perform concatenatio instead of addition
let coercion = numberB + numberA; // 66
console.log(coercion); // concatenation